## David Simpson Apps for Atlassian Cloud - Chrome Extension

Atlassian currently do not provide any ways of adding menu items to user's profile pages

e.g. at

- `https://yoursite.atlassian.net/jira/people/xxxxxxxx`
- `https://yoursite.atlassian.net/wiki/people/xxxxxxxx`

This extension fixes that problem for the [Google Analytics in Jira](https://marketplace.atlassian.com/apps/1222479/google-analytics-in-jira-jsm?hosting=cloud&tab=overview) & [Google Analytics in Confluence](https://marketplace.atlassian.com/apps/1216936/google-analytics-in-confluence?hosting=cloud&tab=overview) apps from [David Simpson Apps](https://dsapps.dev/).

## Adding extension points to Atlassian Jira & Confluence

### What it does:

This extension provides additional menus and links for Atlassian Cloud products

The following extension points exist:

- **Confluence Cloud** - Person Analytics menu item on a person's profile page
- **Jira Cloud** - Person Analytics menu item on a person's profile page

![Screenshot](https://bitbucket.org/davidsimpsonapps/david-simpson-apps-for-atlassian-cloud-chrome-extension/raw/f902289a08f1fcbc432f57639d6546f72f57db6e/src/options/artboard--chrome-extension.png)

### What it doesn't do:

- Anything else
- This browser extension does not track you or even know that it is installed.

## Get it from the Chrome store

### [⬇ Download from the Chrome Store](https://chrome.google.com/webstore/detail/david-simpson-apps-for-at/pegnaebacjjlhmjhbekjibdpoifbbchk/)

---

## How to install (alternative method)

It's always safest to download extensions from the Chrome store, but if you really need this, there's another way...

If you've checked the source code and are happy that this Chrome extension doees not do anything nasty, continue with the directions below.

1. Download the source of this repository as a zip file, from the "..." menu above & selecting "Download repository"
1. Unpack the zip archive into a folder.
1. Open Chrome. Open Extensions from the Window system menu or by pressing the hamburger icon, More Tools, Extensions menu item.
1. Ensure Developer Mode is turned on.
1. Click the Load unpacked button.
1. Select the folder of the unpacked extension.

The extension is now loaded in Chrome.
