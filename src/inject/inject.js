/**
 * Add links to People Analytics on a persons's user profile page
 */

const personAnalyticsExtensionPoints = () => {
  if (
    location.pathname.indexOf("/jira/people/") === 0 ||
    location.pathname.indexOf("/wiki/people/") === 0
  ) {
    // Confluence profiles load more slowly, so we wait longer
    const pause = location.pathname.indexOf("/wiki/") === 0 ? 3000 : 50;
    setTimeout(() => {
      const prefix = location.pathname.indexOf("/wiki/") === 0 ? "/wiki" : "";
      const appKey =
        prefix === "/wiki"
          ? "me.davidsimpson.confluence.addon.google-analytics-for-confluence"
          : "dev.dsapps.jira.addon.google-analytics-in-jira";

      const folders = location.pathname.split("/");
      const atlassianAccountId = folders[folders.length - 1];

      const asideNode = document.querySelector("aside");

      const img = `<img src="data:image/svg+xml,%0A%3Csvg width='20px' height='20px' viewBox='0 0 20 20' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cg id='page' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'%3E%3Cg id='icon' stroke='%2342526E' stroke-width='1.5'%3E%3Cpath d='M16,1 C17.6568542,1 19,2.34314575 19,4 L19,16 C19,17.6568542 17.6568542,19 16,19 L13,19 L13,19 L13,4 C13,2.34314575 14.3431458,1 16,1 Z' id='right'%3E%3C/path%3E%3Cpath d='M10,6 L13,6 L13,6 L13,19 L7,19 L7,9 C7,7.34314575 8.34314575,6 10,6 Z' id='middle'%3E%3C/path%3E%3Cpath d='M4,12 L7,12 L7,12 L7,19 L4,19 C2.34314575,19 1,17.6568542 1,16 L1,15 C1,13.3431458 2.34314575,12 4,12 Z' id='left'%3E%3C/path%3E%3C/g%3E%3C/g%3E%3C/svg%3E" width="20" height="20" style="float:left";/>`;
      asideNode.innerHTML += `<div style="margin: 20px 0;"><a class="aui aui-button" style="display: block; text-align: center; border: 0;" href="${prefix}/plugins/servlet/ac/${appKey}/people?id=${atlassianAccountId}">${img}Person Analytics</a></div>`;
    }, pause);
  }
};

console.info(
  "%c ✔ David Simpson Apps for Atlassian Cloud chrome extension has loaded.",
  "color: rgb(0, 82, 204);"
);

chrome.extension.sendMessage({}, function (response) {
  var readyStateCheckInterval = setInterval(function () {
    if (document.readyState === "complete") {
      clearInterval(readyStateCheckInterval);
      personAnalyticsExtensionPoints();
    }
  }, 10);
});
